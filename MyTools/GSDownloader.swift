//
//  GSDownloader.swift
//  MyTools
//
//  Created by Dhanu Saksrisathaporn on 6/9/2560 BE.
//  Copyright © 2560 20SCOOPS. All rights reserved.
//

import Foundation

extension String {
    static func gsTSVURL(did:String, gid:String)->String {
        return "https://docs.google.com/spreadsheets/d/\(did)/export?gid=\(gid)&format=tsv"
    }
}

class TSV  {
    var rows:[Row]!
    var columns:Row!
    init(string:String) {
        rows = string.components(separatedBy: "\r\n").map { s -> Row in
            return Row(strings: s.components(separatedBy: "\t"))
        }
    }
    
    subscript (_ row:Int,_ column:Int) ->String {
        return rows[row][column]
    }
    subscript (_ row:Int,_ column:String) ->String? {
        if let index = columns[column] {
            return rows[row][index]
        }
        return nil
    }
    var columnCount : Int {
        return columns.count
    }
    var count : Int { // Row count
        return rows.count - 1
    }
}

class Row {
    var data = [String]()
    init(strings:[String]) {
        data = strings
    }
    subscript (_ index:Int) -> String {
        return data[index]
    }
    subscript (_ column:String) -> Int? {
        return data.index(of: column)
    }
    var count : Int {
        return data.count - 1
    }
}

func gsDownload(did:String, gid:String) -> [[String:String]]? {
    if let url = URL(string:.gsTSVURL(did:did,gid:gid)),
        let tsv = try? String(contentsOf: url){
        
        return nil
    }
    return nil
}
