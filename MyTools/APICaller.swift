//
//  APICaller.swift
//  MyTools
//
//  Created by Dhanu Saksrisathaporn on 6/8/2560 BE.
//  Copyright © 2560 20SCOOPS. All rights reserved.
//
import Foundation

enum Failure : String ,Error {
    case nodata = "No data"
    case wrongURL = "Wrong URL"
}

class APICaller : NSObject, URLSessionDelegate {
    var dbProcomToken = ""
    override init() {
        super.init()
    }
    func caller(
        url:String,
        method:String,
        input:[String:Any],
        then: @escaping (_ data:[String:Any]) throws -> Void,
        catchError: @escaping (_ error:Error) -> Void ) {
        
        let urlString = url + ((method == "GET") ? "?\(input.stringFromHttpParameters())" : "")
        
        if let url = URL(string: urlString) {
            
            var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
            
            request.allHTTPHeaderFields = [
                "Authorization":dbProcomToken,
                "Content-Type":"application/json"
            ]
            request.httpMethod = method
            request.httpBody = try? JSONSerialization.data(withJSONObject: input, options: .prettyPrinted)
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    if let error = error {
                        throw error
                    }
                    else if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
                        try then(json)
                    }
                    else {
                        throw Failure.nodata
                    }
                } catch {
                    catchError(error)
                }
            }
            task.resume()
        }
        else {
            catchError(Failure.wrongURL)
        }
    }
    func loginBot(
        then: @escaping (_ data:[String:Any]) throws -> Void,
        catchError: @escaping (_ error:Error) -> Void ) {
        caller(
            url: .dbProcomLoginURL,
            method: "POST",
            input: [
                "email" : String.dbProcomBotEmail ,
                "password" : String.dbProcomBotPass
            ], then: then,
               catchError: catchError)
    }
    
    func addOffer(
        input:[String:Any] ,
        then: @escaping (_ data:[String:Any]) throws -> Void,
        catchError: @escaping (_ error:Error) -> Void ) {
        caller(
            url: .dbProcomAddOfferURL,
            method: "POST",
            input: input,
            then: then,
            catchError: catchError)
    }
    func run(after seconds: Int, closure: @escaping () -> Void) {
        let time = DispatchTime.now() + DispatchTimeInterval.seconds(seconds)
        let queue = DispatchQueue(label: "com.example.runqueue")
        queue.asyncAfter(deadline: time, qos: .background, flags: .inheritQoS) {
            closure()
        }
    }
    func runGenCaller() {
        let sema = DispatchSemaphore(value: 0)
        
        loginBot(
            then: { data in
                print(data)
                if let token = data["token"] {
                    self.dbProcomToken = "Bearer \(token)"
                    sema.signal()
                } else {
                    throw Failure.nodata
                }
        }
        ) { error in
            print("login fail")
            print(error.localizedDescription)
            sema.signal()
        }
        sema.wait()
        
        addOffer(
            input:[
                "land":[
                    "landCode":"1234",
                    "address":"12",
                    "road":"Strasse",
                    "zipcode":"22769",
                    "city":"Hamburg",
                ],
                "price":100000,
                ],
            then: { data in
                print(data)
                sema.signal()
        }
        ) { error in
            print("add fail")
            print(error.localizedDescription)
            sema.signal()
        }
        sema.wait()
        print("CALLER DONE")
    }
}
