//
//  main.swift
//  MyTools
//
//  Created by Dhanu Saksrisathaporn on 3/27/2560 BE.
//  Copyright © 2560 20SCOOPS. All rights reserved.
//

import Foundation
import QuartzCore

func run() {
    print("Welcome to MyTools")
    if CommandLine.arguments.count >= 2 {
        switch CommandLine.arguments[1] {
        case "jl":
            print("JS LANGUAGE DATA GENERATOR")
            runGenerate()
        case "procom":
            APICaller().runGenCaller()
        case "test":
            print("test command")
            CGDisplayMoveCursorToPoint( CGMainDisplayID(), CGPoint(x: 0, y: 0) )
            sleep(1)
            CGDisplayMoveCursorToPoint( CGMainDisplayID(), CGPoint(x: 100, y: 100) )
        default:
            print("please input incorrectly command")
        }
    }
    else {
        print("please input command")
    }
    print("DONE")
}

run()
