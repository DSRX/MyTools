//
//  GenerateFile.swift
//  MyTools
//
//  Created by Dhanu Saksrisathaporn on 3/27/2560 BE.
//  Copyright © 2560 20SCOOPS. All rights reserved.
//

import Foundation
enum InputType:String {
    case tsv
    case csv//bug if have ","
    case unknown
}
enum GenerateType:String {
    case jsonLanguages = "jl"
    case unknown
}
enum GenerateError:Error {
    case requiredInfo([String])
    case wrongInput
    case unknownInput
    case unknown
}
extension GenerateError : LocalizedError {
    var localizedDescription: String {
        switch self {
        case .requiredInfo(let a):
            return "\(a) \(a.count > 1 ? "are":"is") required in info"
        case .wrongInput:
            return "Wrong Input"
        case .unknownInput:
            return "Unknown Input"
        default:
            return "Unknown"
        }
    }
}

typealias FileData = (filename:String,content:String)
typealias RowData = (key:String,data:[String])

let sheetLanguageUrls = [
    ["1fCLH7gvhNS7QEq00MyEf2o68hK6c4r-4po7i4CHXvio","0","language/makler"],
    ["1fCLH7gvhNS7QEq00MyEf2o68hK6c4r-4po7i4CHXvio","1575775495","language/user"],
    ["1fCLH7gvhNS7QEq00MyEf2o68hK6c4r-4po7i4CHXvio","1463112306","language/main"],
    ["1fCLH7gvhNS7QEq00MyEf2o68hK6c4r-4po7i4CHXvio","1359556036","language/share"]
]
func runGenerate() {
    do {
        let currentURL = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
        if CommandLine.arguments.count >= 2 {
            let urlsList = { ()->[[URL]] in
                if CommandLine.arguments.count >= 3
                {
                    return [[
                        CommandLine.arguments[2],
                        CommandLine.arguments.count > 3 ? CommandLine.arguments[3] : ""
                        ].map{ path -> URL in
                            let url = currentURL.appendingPathComponent(path)
                            return FileManager.default.fileExists(atPath: url.path) ? url : URL(fileURLWithPath: path)
                        }]
                }
                else {
                    return sheetLanguageUrls.map { o->[URL] in
                        let destination = currentURL.appendingPathComponent(o[2])
                        return [URL(string:.gsTSVURL(did:o[0],gid:o[1]))!,destination]
                    }
                }
            }()
            try urlsList.forEach{ urls in
                try generateFile(
                    type: GenerateType(rawValue: CommandLine.arguments[1]) ?? .unknown,
                    source: urls[0],
                    destination: urls[1],
                    info: ["prefix":"module.exports = "])
            }
            print("Success")
        }
        else {
            print("wrong input : 'type src des' , 'type src' or just 'type'")
        }
    }
    catch {
        print(error.localizedDescription)
    }
}

func generateFile(type:GenerateType,source:URL,destination:URL,info:[String:String]) throws {
    let check = { () throws -> [FileData] in
        switch type {
        case .jsonLanguages:
            if let prefix = info["prefix"] {
                let inputType = InputType(rawValue: source.pathExtension) ?? .tsv
                print("Downloading.....\(source)")
                let srcString = try { () throws -> String  in
                    if FileManager.default.fileExists(atPath: source.path) {
                        return try String(contentsOf: source, encoding: .utf8)
                    }
                    else if let url = URL(string: source.absoluteString) {
                        return try String(contentsOf: url)
                    }
                    else {
                        throw GenerateError.unknownInput
                    }
                }()
                return try generateJsonLanguage(srcString:srcString,prefix:prefix,inputType:inputType)
            }
            else {
                throw GenerateError.requiredInfo(["prefix"])
            }
        default:
            throw GenerateError.unknown
        }
    }
    try FileManager.default.createDirectory(atPath: destination.path, withIntermediateDirectories: true, attributes: nil)
     try check().forEach { f in
        try f.content.write(to: destination.appendingPathComponent(f.filename), atomically: true, encoding: .utf8)
    }
}

func generateJsonLanguage(srcString:String,prefix:String,inputType:InputType) throws -> [FileData] {
    let  data = try srcString
        .components(separatedBy: "\r\n")
        .map { s -> RowData in
            switch inputType {
            case .tsv:
                let rows = s.components(separatedBy: "\t")//.filter{ s in return s.characters.count > 0 }
                return (rows[0], rows.count > 1 ? Array(rows[1...(rows.count-1)])  : [])
            case .csv:
                let rows = s.components(separatedBy: ",")//.filter{ s in return s.characters.count > 0 }
                return (rows[0], rows.count > 1 ? Array(rows[1...(rows.count-1)])  : [])
            default:
                throw GenerateError.wrongInput
            }
    }
    let content = data[1...(data.count-1)]
    let pathExtension = data[0].key
    let filenames = data[0].data
    
    return filenames.enumerated().map { offset,filename -> FileData  in
        return ( "\(filename).\(pathExtension)",
            content.reduce(prefix + "{\n") { (r, d) -> String in
                if d.data.count > 0 ,
                    d.data[offset].characters.count > 0 {
                    return r + "\"\(d.key)\":\"\(d.data[offset])\",\n"
                }
                else {
                    return r + "\n//\(d.key)\n"
                }
            } + "}"
        )
    }
}
