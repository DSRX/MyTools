//
//  AppRunner.swift
//  MyTools
//
//  Created by Dhanu Saksrisathaporn on 4/22/2560 BE.
//  Copyright © 2560 20SCOOPS. All rights reserved.
//

import Foundation
import AppKit

func runProcom() {
    if let url = URL(string: "http://35.158.26.64/") {
        NSWorkspace.shared().open(url)
    }
    var notyet = true
    while notyet {
        NSWorkspace.shared().runningApplications.forEach { app in
            if let appName = app.bundleURL?.lastPathComponent,
                (appName.contains("Chrome") || appName.contains("Safari")){
                notyet = false
                manageProcom(app: app)
            }
        }
        if notyet {
            sleep(1)
            print("nah")
        }
    }
}

func manageProcom(app:NSRunningApplication) {
    if let localAppName = app.localizedName {
        print(localAppName)
        
        let windowList = CGWindowListCopyWindowInfo(.optionOnScreenOnly, kCGNullWindowID)
        if let windows = windowList as? [[String:Any]] {
            for window in windows {
                print("name = \(window["kCGWindowOwnerName"] ?? "null")")
                print("pId = \(window["kCGWindowOwnerPID"] ?? "null")")
                if let name = window["kCGWindowOwnerName"] as? String,
                    let pId = window["kCGWindowOwnerPID"] as? Int ,
                    name.contains("Chrome") {
                    print("name = \(name)")
                    print("pId = \(pId)")
                    print(window)
                }
            }
        }
  
//        for (NSMutableDictionary* entry in (NSArray*)windowList)
//        {
//            NSString* ownerName = [entry objectForKey:(id)kCGWindowOwnerName];
//            NSInteger ownerPID = [[entry objectForKey:(id)kCGWindowOwnerPID] integerValue];
//            NSLog(@"%@:%d", ownerName, ownerPID);
//        }
    }
}
